msgid ""
msgstr ""
"Project-Id-Version: gpxpod\n"
"Report-Msgid-Bugs-To: translations\\@example.com\n"
"POT-Creation-Date: 2019-07-25 01:07+0200\n"
"PO-Revision-Date: 2019-07-24 23:10\n"
"Last-Translator: Julien Veyssier (eneiluj)\n"
"Language-Team: Finnish\n"
"Language: fi_FI\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: gpxpod\n"
"X-Crowdin-Language: fi\n"
"X-Crowdin-File: /master/translationfiles/templates/gpxpod.pot\n"

#: /var/www/html/n16/apps/gpxpod/appinfo/app.php:47
#: /var/www/html/n16/apps/gpxpod/specialAppInfoFakeDummyForL10nScript.php:2
msgid "GpxPod"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/controller/pagecontroller.php:1565
msgid "There was an error during \"gpxelevations\" execution on the server"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/filetypes.js:51
#: /var/www/html/n16/apps/gpxpod/js/filetypes.js:106
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4433
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4470
msgid "Directory {p} has been added"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/filetypes.js:58
#: /var/www/html/n16/apps/gpxpod/js/filetypes.js:113
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4445
msgid "Failed to add directory"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/filetypes.js:67
#: /var/www/html/n16/apps/gpxpod/js/filetypes.js:121
msgid "View in GpxPod"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:990
msgid "File"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:991
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1483
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:3962
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4025
msgid "download"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:996
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1501
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5076
msgid "This public link will work only if '{title}' or one of its parent folder is shared in 'files' app by public link without password"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1003
msgid "Draw track"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1008
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1011
msgid "metadata link"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1015
msgid "tracks/routes name list"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1030
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:421
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:114
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:115
msgid "Distance"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1040
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1614
msgid "Duration"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1043
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:116
msgid "Moving time"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1046
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:117
msgid "Pause time"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1066
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:123
msgid "Begin"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1069
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:124
msgid "End"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1072
#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:268
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:426
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:121
msgid "Cumulative elevation gain"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1076
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:122
msgid "Cumulative elevation loss"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1080
msgid "Minimum elevation"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1084
msgid "Maximum elevation"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1088
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:120
msgid "Maximum speed"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1099
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:119
msgid "Average speed"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1110
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:118
msgid "Moving average speed"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1120
msgid "Moving average pace"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1136
msgid "{n} track"
msgid_plural "{n} tracks"
msgstr[0] ""
msgstr[1] ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1137
msgid "{np} picture"
msgid_plural "{np} pictures"
msgstr[0] ""
msgstr[1] ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1200
msgid "Track \"{tn}\" is loading"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1294
msgid "Failed to delete track"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1295
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1349
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1376
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4245
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4387
msgid "Reload this page"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1297
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1351
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1378
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4247
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4299
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4378
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4389
msgid "Error"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1309
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1363
msgid "Successfully deleted"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1311
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1365
msgid "You can restore deleted files in \"Files\" app"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1315
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1369
msgid "Impossible to delete"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1320
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1348
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1375
msgid "Failed to delete selected tracks"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1441
msgid "Click the color to change it"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1443
msgid "Deselect to hide track drawing"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1449
msgid "Select to draw the track"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1491
msgid "More"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1495
msgid "Center map on this track"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1511
msgid "Delete this track file"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1517
msgid "Correct elevations with smoothing for this track"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1522
msgid "Correct elevations for this track"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1531
msgid "View this file in GpxMotion"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1540
msgid "Edit this file in GpxEdit"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1549
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:3818
msgid "no date"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1581
msgid "No track visible"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1586
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4886
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4899
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:288
msgid "Tracks from current view"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1589
msgid "All tracks"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1605
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:170
msgid "Draw"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1607
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1863
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2698
#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:198
msgid "Track"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1609
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:416
msgid "Date"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1611
msgid "Dist<br/>ance<br/>"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1616
msgid "Cumulative<br/>elevation<br/>gain"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1722
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:74
msgid "distance"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1724
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2598
msgid "altitude/distance"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1727
msgid "speed/distance"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1730
msgid "pace(time for last km or mi)/distance"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1866
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2701
msgid "Link"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1869
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2704
#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:322
msgid "Elevation"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1872
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2707
msgid "Latitude"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1873
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2708
msgid "Longitude"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1876
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2080
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2384
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2711
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2775
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2936
msgid "Comment"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1880
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2715
msgid "Description"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:1884
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:2719
msgid "Symbol name"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:3251
msgid "Database has been cleaned"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:3254
msgid "Impossible to clean database"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:3372
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:20
msgid "Folder"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:3384
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5129
msgid "Public link to '{folder}' which will work only if this folder is shared in 'files' app by public link without password"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:3960
msgid "Public folder share"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4023
msgid "Public file share"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4071
msgid "Server name or server url should not be empty"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4072
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4077
msgid "Impossible to add tile server"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4076
msgid "A server with this name already exists"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4109
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:465
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:509
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:554
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:603
msgid "Delete"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4142
msgid "Tile server \"{ts}\" has been added"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4145
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4149
msgid "Failed to add tile server \"{ts}\""
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4183
msgid "Tile server \"{ts}\" has been deleted"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4186
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4190
msgid "Failed to delete tile server \"{ts}\""
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4244
msgid "Failed to restore options values"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4298
msgid "Failed to save options values"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4371
msgid "Destination directory is not writeable"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4374
msgid "Destination directory does not exist"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4377
#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4386
msgid "Failed to move selected tracks"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4396
msgid "Following files were moved successfully"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4398
msgid "Following files were NOT moved"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4481
msgid "There is no compatible file in {p} or any of its sub directories"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4492
msgid "Failed to recursively add directory"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4513
msgid "Directory {p} has been removed"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:4524
msgid "Failed to remove directory"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5037
msgid "Public link to the track"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5041
msgid "Public link to the folder"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5255
msgid "Select at least one track"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5259
msgid "Destination folder"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5262
msgid "Origin and destination directories must be different"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5287
msgid "Are you sure you want to delete the track {name} ?"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5290
msgid "Confirm track deletion"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5309
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:45
msgid "Add directory"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxpod.js:5320
msgid "Add directory recursively"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:187
msgid "better in"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:192
msgid "worse in"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:202
msgid "Divergence details"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:210
msgid "Divergence distance"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:214
msgid "is shorter than"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:224
msgid "is longer than"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:240
msgid "Divergence time"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:244
msgid "is quicker than"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:253
msgid "is slower than"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:273
msgid "is less than"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:283
msgid "is more than"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:295
msgid "There is no divergence here"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:299
msgid "Segment details"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:301
msgid "Segment id"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:302
msgid "From"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:305
msgid "To"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:320
msgid "Time"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/gpxvcomp.js:382
msgid "Click on a track line to get details on the section"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/jquery.colorbox-min.js:6
msgid "LoadedContent"
msgid_plural "width:0; height:0; overflow:hidden; visibility:hidden"
msgstr[0] ""
msgstr[1] ""

#: /var/www/html/n16/apps/gpxpod/js/leaflet.js:5
msgid "left"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/js/leaflet.js:5
msgid "right"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/specialAppInfoFakeDummyForL10nScript.php:3
msgid " "
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:4
msgid "Folder and tracks selection"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:5
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:398
msgid "Settings and extra actions"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:6
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:640
msgid "About GpxPod"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:22
msgid "Choose a folder"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:40
msgid "Reload and analyze all files in current folder"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:41
msgid "Reload current folder"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:44
msgid "Add directories recursively"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:46
msgid "Delete current directory"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:52
msgid "There is no directory in your list"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:55
msgid "Add one to be able to see tracks"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:62
msgid "Options"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:70
msgid "Map options"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:74
msgid "Display markers"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:77
msgid "Show pictures markers"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:79
msgid "Only pictures with EXIF geolocation data are displayed"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:83
msgid "Show pictures"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:86
msgid "With this disabled, public page link will include option to hide sidebar"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:90
msgid "Enable sidebar in public pages"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:93
msgid "Open info popup when a track is drawn"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:96
msgid "Auto-popup"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:99
msgid "If enabled :"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:101
msgid "Zoom on track when it is drawn"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:103
msgid "Zoom to show all track markers when selecting a folder"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:105
msgid "If disabled :"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:107
msgid "Do nothing when a track is drawn"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:109
msgid "Reset zoom to world view when selecting a folder"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:112
msgid "Auto-zoom"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:114
msgid "Display elevation or speed chart when a track is drawn"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:118
msgid "Display chart"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:121
msgid "Timezone"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:126
msgid "Measuring units"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:128
msgid "Metric"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:129
msgid "English"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:130
msgid "Nautical"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:136
msgid "Track drawing options"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:137
msgid "Use symbols defined in the gpx file"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:141
msgid "Gpx symbols"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:144
msgid "Draw black borders around track lines"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:148
msgid "Line borders"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:150
msgid "Track line width in pixels"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:152
msgid "Line width"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:160
msgid "Display routes points"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:163
msgid "Show direction arrows along lines"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:167
msgid "Direction arrows"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:172
msgid "track+waypoints"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:173
msgid "track"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:174
msgid "waypoints"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:179
msgid "Waypoint style"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:185
msgid "Tooltip"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:187
msgid "on hover"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:188
msgid "permanent"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:194
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:197
msgid "Enables tracks coloring by the chosen criteria"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:195
msgid "Color tracks by"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:198
msgid "none"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:199
msgid "speed"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:200
msgid "elevation"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:201
msgid "pace"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:202
msgid "extension"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:208
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:211
msgid "Enables tracks coloring by the chosen extension value"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:209
msgid "Color tracks by extension value"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:215
msgid "IGC elevation track"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:217
msgid "Both GNSS and pressure"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:218
msgid "Pressure"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:219
msgid "GNSS"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:225
msgid "Table options"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:226
msgid "Table only shows tracks that are inside current map view"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:230
msgid "Dynamic table"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:233
msgid "For slow connections or if you have huge files, a simplified version is shown when hover"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:237
msgid "Simplified previews"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:241
msgid "Enables transparency when hover on table rows to display track overviews"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:245
msgid "Transparency"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:250
msgid "Storage exploration options"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:255
msgid "Display tracks recursively in selected folder"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:262
msgid "Display shared folders/files"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:269
msgid "Explore external storages"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:276
msgid "Display folders containing pictures only"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:281
msgid "Effective on future actions"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:291
msgid "what determines if a track is shown in the table :"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:293
msgid "crosses : at least one track point is inside current view"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:295
msgid "begins : beginning point marker is inside current view"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:297
msgid "track N,S,E,W bounds intersect current view bounds square"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:301
msgid "List tracks that"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:304
msgid "cross current view"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:305
msgid "begin in current view"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:306
msgid "have N,S,E,W bounds crossing current view"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:311
msgid "Select visible"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:314
msgid "Deselect all"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:317
msgid "Deselect visible"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:320
msgid "Delete selected"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:323
msgid "Move selected tracks to"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:327
msgid "Hide elevation profile"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:331
msgid "Compare selected tracks"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:402
msgid "Filters"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:405
msgid "Clear"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:409
msgid "Apply"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:417
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:422
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:427
msgid "min"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:418
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:423
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:428
msgid "max"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:435
msgid "Custom tile servers"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:438
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:478
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:521
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:566
msgid "Server name"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:439
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:479
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:522
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:567
msgid "For example : my custom server"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:440
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:480
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:523
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:568
msgid "Server url"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:441
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:524
msgid "For example : http://tile.server.org/cycle/{z}/{x}/{y}.png"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:442
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:482
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:525
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:570
msgid "Min zoom (1-20)"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:444
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:484
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:527
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:572
msgid "Max zoom (1-20)"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:446
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:490
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:535
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:584
msgid "Add"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:449
msgid "Your tile servers"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:475
msgid "Custom overlay tile servers"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:481
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:569
msgid "For example : http://overlay.server.org/cycle/{z}/{x}/{y}.png"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:486
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:574
msgid "Transparent"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:488
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:576
msgid "Opacity (0.0-1.0)"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:493
msgid "Your overlay tile servers"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:518
msgid "Custom WMS tile servers"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:529
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:578
msgid "Format"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:531
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:580
msgid "WMS version"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:533
#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:582
msgid "Layers to display"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:538
msgid "Your WMS tile servers"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:563
msgid "Custom WMS overlay servers"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:587
msgid "Your WMS overlay tile servers"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:616
msgid "Clean files or database"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:617
msgid "Delete all '.marker' and '.geojson' files"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:618
msgid "Delete '.markers' and '.geojson' files corresponding to existing gpx files"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:619
msgid "Metadata will be generated again on folder load"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:619
msgid "Delete metadata for all tracks in the database (distance, duration, average speed...)"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:624
msgid "deleting"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:631
msgid "Append \"&track=all\" to the link to display all tracks when public folder page loads."
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:642
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:143
msgid "Shortcuts"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:644
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:145
msgid "toggle sidebar"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:645
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:146
msgid "toggle minimap"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:648
#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:150
msgid "Features"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:711
msgid "Documentation"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:719
msgid "Source management"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxcontent.php:733
msgid "Authors"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:12
msgid "Upload gpx files to compare"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:27
msgid "Compare"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:48
msgid "File pair to compare"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:60
msgid "and"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:67
msgid "Color by"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:71
msgid "time"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:77
msgid "cumulative elevation gain"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:99
msgid "Global stats on loaded tracks"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:105
msgid "stat name / track name"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:125
msgid "Number of points"
msgstr ""

#: /var/www/html/n16/apps/gpxpod/templates/gpxvcompcontent.php:141
msgid "About comparison"
msgstr ""

